package edu.towson.cosc431.SINGH.todos

import android.app.Activity
import android.content.ComponentName
import android.content.Intent
import android.graphics.Paint
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.todo_view.*
import java.time.LocalDate

class MainActivity : AppCompatActivity(),IController {



    override fun removeTodo(idx: Int) {

        val Todo = taskList.get(idx)
        taskList.removeAt(idx)
    }

    override fun addtodo(todo: TodoModel) {
        taskList.add(todo)
        // TODO - handle UI update
    }

    override fun toggleComplete(idx: Int) {


        val todo = taskList.get(idx)

        //var tv: TextView = findViewById(R.id.taskName)

        taskList.set(idx, todo.copy(  isComplete = !todo.isComplete))
        // TODO - handle UI update

        taskName.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG

    }

    var taskList: MutableList<TodoModel> = mutableListOf()

    @RequiresApi(Build.VERSION_CODES.O)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val TaskAdapter= TodoAdapter(taskList, this)

        recyclerView.layoutManager= LinearLayoutManager(this)

        recyclerView.adapter=TaskAdapter


        add_btn.setOnClickListener { launchNewTodoActivity() }

        populatetodoList()

        val itemHelper = TaskListHelper(TaskListCallback(recyclerView.adapter as TodoAdapter))
        itemHelper.attachToRecyclerView(recyclerView)


    }

    private fun launchNewTodoActivity() {
        val intent = Intent()
        intent.component = ComponentName(this, NewTodoActivity::class.java)
        startActivityForResult(intent, List)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            List -> {
                when(resultCode){
                    Activity.RESULT_OK -> {
                        Log.d("MainActivity", "Result is Okay")
                        val json = data?.extras?.get(NewTodoActivity.TODO) as String?
                        if(json != null){
                            val todoasJson = Gson().fromJson<TodoModel>(json, TodoModel::class.java)
                            Log.d("MainActivity", todoasJson.toString())
                            taskList.add(todoasJson)
                            recyclerView.adapter?.notifyDataSetChanged()

                        }
                    }
                    Activity.RESULT_CANCELED -> {
                        Log.d("MainActivity", "Result was Cancelled")
                    }
                }
            }
        }
    }

    companion object {
        val List = 100
    }

    @RequiresApi(Build.VERSION_CODES.O)

    private fun populatetodoList() {
        (1..10).forEach {

            taskList.add(TodoModel
            (       "Title"+it,
                    "Contents"+it,
                    it%3==0,
                     date = LocalDate.now()))

        }
    }
}
