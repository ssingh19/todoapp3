package edu.towson.cosc431.SINGH.todos

interface IController {
    // TODO - update interface to handle recyclerview

    fun toggleComplete(idx: Int)
    fun addtodo(todo: TodoModel)
    fun removeTodo(idx:Int)

}