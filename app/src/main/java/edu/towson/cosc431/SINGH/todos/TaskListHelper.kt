package edu.towson.cosc431.SINGH.todos

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper


class TaskListHelper(callback: ItemTouchHelper.Callback) : ItemTouchHelper(callback)

class TaskListCallback(val adapter: TodoAdapter) : ItemTouchHelper.Callback() {
    override fun getMovementFlags(p0: RecyclerView, p1: RecyclerView.ViewHolder): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
        return ItemTouchHelper.Callback.makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder): Boolean {
        return adapter.onItemMove(p1?.adapterPosition!!, p2?.adapterPosition!!)
    }

    override fun onSwiped(p0: RecyclerView.ViewHolder, p1: Int) {
        adapter.onItemDelete(p0?.adapterPosition)
    }


    override fun isItemViewSwipeEnabled(): Boolean {
            return true
        }

        override fun isLongPressDragEnabled(): Boolean {
            return true
        }
    }
