package edu.towson.cosc431.SINGH.todos

import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.todo_view.view.*
import java.time.LocalDate
import java.util.*


class TodoAdapter(val taskList:MutableList<TodoModel>, val controller:IController) :RecyclerView.Adapter<TodoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        //1. Inflate a view
        val view = LayoutInflater.from(parent.context).inflate(R.layout.todo_view, parent, false)


        //2. Create and return the ViewHolder
        val viewHolder = TodoViewHolder(view)

        //Handle the events
        viewHolder.itemView.isCompletedCB.setOnClickListener {

            val position = viewHolder.adapterPosition

            controller.toggleComplete(position)
            notifyItemChanged(position)
        }

        /*viewHolder.itemView?.setOnLongClickListener {
            val position = viewHolder.adapterPosition
            controller.removeTodo(position)
            notifyItemRemoved(position)

            notifyDataSetChanged()
            true

        }
*/

        return viewHolder
    }

    override fun getItemCount(): Int {
        return taskList.size


    }
    @RequiresApi(Build.VERSION_CODES.O)

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {


        //1. get the song at the position
        val todo= taskList.get(position)

        //2. set the view properties

        holder.itemView.taskName.text=todo.title

        holder.itemView.Information.text=todo.contents

        holder.itemView.date_due.text= LocalDate.now().toString()

        holder.itemView.isCompletedCB.isChecked = todo.isComplete


    }

    fun onItemDelete(position: Int?) {
        taskList?.removeAt(position!!)
        notifyItemRemoved(position!!)
    }

    fun onItemMove(from: Int, to: Int): Boolean {
        if (from < to) {
            for (i in from until to) {
                Collections.swap(taskList, i, i + 1)
            }
        } else {
            for (i in from downTo to + 1) {
                Collections.swap(taskList, i, i - 1)
            }
        }
        notifyItemMoved(from, to)
        return true
    }
}

class TodoViewHolder(view: View) : RecyclerView.ViewHolder(view){

}