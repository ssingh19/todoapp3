package edu.towson.cosc431.SINGH.todos

import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.support.annotation.RequiresApi
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_new_todo.*
import java.time.LocalDate
import java.util.*

class NewTodoActivity : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.O)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)

        Savebtn.setOnClickListener { sendSaveBack() }

        }

    @RequiresApi(Build.VERSION_CODES.O)

    private fun sendSaveBack() {

        val data = Intent()

        val title = titleET.editableText.toString()
        val content = contentET.editableText.toString()
        val isCompleted = checkBox.isChecked
        val todo = TodoModel(title, content, isCompleted, date = LocalDate.now())

        val gson= Gson()

        val todoAsJson= gson.toJson(todo)
        data.putExtra(TODO, todoAsJson)

        setResult(RESULT_OK, data)

        finish()
    }


    companion object {
        val TODO = "Todo"
    }
}
